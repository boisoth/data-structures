const reverseWord = word => {
  let reversed = "";
  for (let letter of word) {
    reversed = letter + reversed;
  }
  return reversed;
};
module.exports = reverseWord;
