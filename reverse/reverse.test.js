const reverse = require("./reverse");

describe("Reverse String Test", () => {
  test("It should reverse the word", () => {
    expect(reverse("Hello")).toBe("olleH");
  });
});
