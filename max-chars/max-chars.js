// Take a given string, and check the max chars
// 1. Iterate through string as an array and map to a hash table
// 2. If the hash table key exists, add 1 to the value
// 3. If it hash table key doesn't exist, set the value to 1
// 4. Now, Iterate through the items of the hash table to set the max

const maxChars = str => {
  const charsMap = {};
  let max = 0;
  let maxChar = "";

  // Iterate through str and set the key/value or update it
  for (let char of str) {
    if (charsMap[char]) {
      charsMap[char]++;
    } else {
      charsMap[char] = 1;
    }
  }

  // Iterate through charsMap and return max
  for (let char in charsMap) {
    if (charsMap[char] > max) {
      max = charsMap[char];
      maxChar = char;
      console.log(
        `Most characters is: ${maxChar.toUpperCase()}: ${max} times.`
      );
    }
  }
};

maxChars("alabamassw");

module.exports = maxChars;
