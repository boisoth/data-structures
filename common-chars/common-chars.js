/**
 * #1. Check two given arrays if they share a common character
 * #2. First attempt, use brute force
 * #3. If data sets has a limit, and memory complexity is not a problem:
 *   -- Nest for loop O(n^2)
 * #4. If memory is a problem, create a hash table
 *   -- map chars, and one another loop after O(n+m) or O(1) reduced
 */

// const commonChars = (arr1, arr2) => {
//   // Check if each item in arr1 matches with arr2
//   for (let i = 0; i < arr1.length; i++) {
//     for (let j = 0; j < arr2.length; j++) {
//       /**Example:
//        * arr1[0] == arr2[0],
//        * arr1[0] == arr2[1],
//        * arr1[0] == arr2[2]
//        *  */
//       if (arr1[i] === arr2[2]) {
//         // return true if match
//         return true;
//       }
//     }
//   }
//   // return false if no match
//   return false;
// };

const commonChars = (arr1, arr2) => {
  let map = {};

  for (let i = 0; i < arr1.length; i++) {
    console.log(i);
    if (!map[i]) {
      // Set the value of each map property
      map[arr1[i]] = true;
    }
  }

  for (let j = 0; j < arr2.length; j++) {
    if (map.hasOwnProperty(arr2[j])) {
      console.log("Has common char: " + arr2[j]);
      return true;
    }
  }

  return false;
};
module.exports = commonChars;
