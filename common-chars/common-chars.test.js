const commonChars = require("./common-chars");

describe("Common Char Test", () => {
  test("Should return true if both arrays have a common character", () => {
    expect(
      commonChars(["a", "x", "c", "g"], ["t", "c", "v", "l"])
    ).toBeTruthy();
  });
});
