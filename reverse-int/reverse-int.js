const reverseInt = n => {
  const reverse = n
    .toString()
    .split("")
    .reverse()
    .join("");

  return Math.sign(n) * parseInt(reverse);
};

module.exports = reverseInt;
