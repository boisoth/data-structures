const reverseInt = require("./reverse-int");

describe("Reverse Int Test", () => {
  test("Should take in a number and reverse it", () => {
    expect(reverseInt(-25)).toEqual(-52);
  });
  console.log("yes");
});
