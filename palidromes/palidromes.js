const palidromes = str => {
  const isPalidrome = str
    .split("")
    .reverse()
    .join("");
  return isPalidrome === str;
};

module.exports = palidromes;
